package HillCipher;

import com.google.common.primitives.Doubles;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealMatrix;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class HillCipher {

    private static final int MATRIX_ROWS = 3;
    private static final double[][] KEY = {{1d, -5d, 10d}, {11d, -6d, 5d}, {-9d, 15d, 4d}};
    private static final RealMatrix KEY_MATRIX = new Array2DRowRealMatrix(KEY);

    public static void main(String[] args) {
        validateInput(args);
        List<RealMatrix> encryptedMatrix = encrypt(args[0]);
        List<RealMatrix> decryptedMatrix = decrypt(encryptedMatrix);
        printResult(encryptedMatrix, decryptedMatrix);
    }

    private static void printResult(List<RealMatrix> encryptedMatrix, List<RealMatrix> decryptedMatrix) {
        System.out.println("Encrypted message: " + getEncryptedString(encryptedMatrix) +
                "\nDecrypted result: " + getDecryptedString(decryptedMatrix));
    }

    private static String getDecryptedString(List<RealMatrix> decryptedMatrix) {
        return new String(covertToBytes(decryptedMatrix));
    }

    private static String getEncryptedString(List<RealMatrix> encryptedMatrix) {
        return getAsciiIntegers(encryptedMatrix).stream()
                .map(Object::toString)
                .collect(Collectors.joining(", "));
    }

    private static List<RealMatrix> encrypt(String input) {
        List<RealMatrix> vectorMatrix = getVectors(convertToNumberAscii(input));
        return vectorMatrix.stream()
                .map(KEY_MATRIX::multiply)
                .collect(Collectors.toList());
    }

    private static List<RealMatrix> decrypt(List<RealMatrix> encrypedMatrix) {
        RealMatrix inversedMatrix = new LUDecomposition(KEY_MATRIX).getSolver().getInverse();
        return encrypedMatrix.stream()
                .map(inversedMatrix::multiply)
                .collect(Collectors.toList());
    }

    private static List<RealMatrix> getVectors(double[] ascii) {
        List<RealMatrix> vectors = new ArrayList<>();
        for (int i = 0; i < ascii.length; i = i + MATRIX_ROWS) {
            double[] vector = Arrays.copyOfRange(ascii, i, i + MATRIX_ROWS);
            vectors.add(new Array2DRowRealMatrix(vector));
        }
        return vectors;
    }

    private static List<Integer> getAsciiIntegers(List<RealMatrix> matrices) {
        List<Double> doubles = new ArrayList<>();
        for (RealMatrix matrix : matrices) {
            doubles.addAll(Doubles.asList(matrix.getColumn(0)));
        }
        return doubles.stream()
                .map(d -> BigDecimal.valueOf(d).setScale(0, RoundingMode.HALF_UP).intValue())
                .filter(integer -> integer > 0)
                .collect(Collectors.toList());
    }

    private static double[] convertToNumberAscii(String input) {
        byte[] asciiBytes = getAsciiBytes(input);
        int remaining = asciiBytes.length % MATRIX_ROWS;
        int length = remaining == 0 ? asciiBytes.length : asciiBytes.length + MATRIX_ROWS - remaining;
        double[] converted = new double[length];
        for (int i = 0; i < asciiBytes.length; i++) {
            converted[i] = (double) asciiBytes[i];
        }
        return converted;
    }

    private static byte[] covertToBytes(List<RealMatrix> matrices) {
        List<Integer> asciiNumbers = getAsciiIntegers(matrices);
        byte[] asciiCode = new byte[asciiNumbers.size()];
        for (int i = 0; i < asciiNumbers.size(); i++) {
            int intValue = asciiNumbers.get(i);
            byte asciiNumber = (byte) intValue;
            asciiCode[i] = asciiNumber;
        }
        return asciiCode;
    }

    private static byte[] getAsciiBytes(String input) {
        try {
            return input.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Input could not be converted to ASCII.");
        }
    }

    private static void validateInput(String[] input) {
        if (input == null || input.length == 0) {
            throw new RuntimeException("Input can't be empty. Enter phrase to encrypt.");
        }
    }

}
